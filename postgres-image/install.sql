DROP TABLE IF EXISTS sci_user;
CREATE TABLE sci_user(
    userID SERIAL NOT NULL,
    name VARCHAR NOT NULL,
    home_location VARCHAR NOT NULL,
    avatar VARCHAR NOT NULL,
    statistic_offerCount INT NOT NULL DEFAULT 0,
    statistic_servitudeCount INT NOT NULL DEFAULT 0,
    statistic_foodDonationCount INT NOT NULL DEFAULT 0,
    lastActivityTime TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01',
    creationDay TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01',
    password VARCHAR(100) NOT NULL,
    token VARCHAR(100),
    PRIMARY KEY(userID)
);

INSERT INTO sci_user(name, home_location, avatar, statistic_offerCount, statistic_servitudeCount, statistic_foodDonationCount, lastActivityTime, creationDay, password)
    VALUES
    -- Password: jensisttoll
    ('Alpaka', 'Weinhof 7, 89073 Ulm', 'images/Alpaka_Avatar.jpg', 13, 5, 4, '2019-11-02T16:11:05.516Z', '2019-11-02T16:11:05.516Z', '$2a$10$oLx1tTTowkfqtEJ12gUZJu69Yj8lQQahmyHgFaXsxUTxLxyifEBIW');


DROP TABLE IF EXISTS sci_inventory;
CREATE TYPE item_type_type AS ENUM ('food', 'packet-handling', 'rental', 'service');
CREATE TABLE sci_inventory(
    inventoryID SERIAL NOT NULL,
    userID SERIAL NOT NULL REFERENCES sci_user(userID),
    username VARCHAR(100) NOT NULL,
    item_type item_type_type,
    item_data JSON NOT NULL,
    item_name VARCHAR(100) NOT NULL,
    item_location POINT NOT NULL,
    item_location_human_readable_address VARCHAR(100) NOT NULL,
    item_timestamp TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:01',
    isOffer BOOLEAN DEFAULT FALSE,
    -- categoryID SERIAL NOT NULL REFERENCES sci_category(categoryID),
    PRIMARY KEY(inventoryID)
);

INSERT INTO sci_inventory(userID, username, item_type, item_name, item_data, item_location, item_location_human_readable_address, item_timestamp, isOffer)
    VALUES
    (42, Alpaka, "food", "Gnocchi Gorgonzola", '{"food_type":"pasta","food_preparation_time":1572702719457,"food_healthy_status":7}', (48.39723, 9.99393), "Neue Straße 59, 89073 Ulm", '2019-11-02T13:48:01.961Z', 1);
