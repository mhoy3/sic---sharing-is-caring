const uuid = require("uuid/v4");
const bcrypt = require('bcrypt');
const bcryptSaltRounds = 10;

class DatabaseWrapper {
    static async getInventoryList(db) {
        let sql = `SELECT COUNT(*) FROM sci_inventory`;
        let results = await db.query(sql);

        console.log(results);

        return results.rows;
    }

    static async claimInventory(db, inv_id) {
        inv_id = Number.parseInt(inv_id);
        console.log(db);

        let sql = `UPDATE sci_inventory SET isOffer = false WHERE inventoryID = ${inv_id} AND isOffer = true;`
        let result = await db.query(sql);

        return result.rowCount == 1;
    }

    static login(db, username, password) {
        return new Promise((resolve, reject) => {
            db.query({
                text: "SELECT * FROM sci_user WHERE name=$1",
                values: [username]
            }, (err, res) => {
                if (err) {
                    resolve({
                        error: err,
                        valid: false
                    })
                } else {
                    if (!bcrypt.compareSync(self.hashPassword(password), res.rows[0].password)) {
                        resolve({
                            valid: false,
                            error: "Invalid credentials"
                        })
                    } else {
                        var userID = res.rows[0].userID;
                        var token = uuid();
                        resolve({
                            valid: true,
                            token: token
                        })
                        db.query({
                            text: "UPDATE sci_user SET token = $1 WHERE userID = $2",
                            values: [token, userID]
                        })
                    }
                }
            })
        })
    }

    static hashPassword(userPassword) {
        let salt = bcrypt.genSaltSync(bcryptSaltRounds);

        return bcrypt.hashSync(userPassword, salt);
    }

    static createUser(db, username, password, location){
        return new Promise(resolve => {
            db.query({
                text: "SELECT userID from sci_user WHERE name=$1",
                values: [username]
            }, (err, res) => {
                if(err){
                    resolve({
                        error: err,
                        valid: false
                    })
                }else{
                    if(res.rows.length <= 0){
                        var avatar = "images/Alpaka_Avatar.jpg"
                        var curTime = new Date().toISOString();
                        password = this.hashPassword(password)
                        db.query({
                            text: `INSERT INTO sci_user VALUES ($1, $2, ${avatar}, 0, 0, 0, ${curTime}, ${curTime}, $3)`,
                            values: [username, location, password]
                        }, (err2, res2) => {
                            if(err){
                                resolve({
                                    error: err,
                                    valid: false
                                })
                            }else{
                                resolve({
                                    valid: true,
                                })
                            }
                        })
                    }else{
                        resolve({
                            error: "User already exists",
                            valid: false
                        })
                    }
                }
            })
        })
    }
}

module.exports = DatabaseWrapper;