const JSONPath = "./user.json";

async function getData(path){
    let fetching = await fetch(path);
    let response = await fetching.json();
    return response;
}

getData(JSONPath).then(d => {
    document.querySelector("#avatar").src = d.avatar;
    document.querySelector("#addr").innerHTML = d.home_location;
    document.querySelector("#usrname").innerHTML = d.name;
    document.querySelector("#verli").innerHTML = d.statistic.offerCount;
    document.querySelector("#diens").innerHTML = d.statistic.servitudeCount;
    document.querySelector("#essen").innerHTML = d.statistic.foodDonationCount;
});