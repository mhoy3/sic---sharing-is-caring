const express = require('express')
const bodyParser = require("body-parser")
const path = require("path")
const { Authorizer } = require("./authorization")
const databaseWrapper = require('./databaseWrapper');
const OSM = require('./osmUtil');


const app = express()
const { Client } = require('pg');
const pg = new Client();

const log = (msg, level = 'log') => {
  let logging = [msg];
  logging.unshift((new Date()).toLocaleString());

  console[level](logging.join(': '));
}


const sleep = sleep_ms => {
  return new Promise(res => {
    setTimeout(res, sleep_ms);
  });
}


(async function () {
  await sleep(6000);
  await pg.connect();

  // Parses the body of a POST request if it is in json format
  app.use(bodyParser.json())
  app.use(express.json())
  app.use(express.urlencoded({ extended: true }))
  app.use(express.static(__dirname + "/public"))

  const auth = new Authorizer(pg);

  app.get('/databaseHealz', async function (req, res) {
    log('Querying database for current status!');
    res.json((await pg.query('SELECT VERSION()')).rows[0]);
  });

  /*app.get('/test', async function (req, res) {
    res.json({ streetName: await OSM.getStreetName(48.39624, 9.99034) });
  });*/

  app.get('/', async function (req, res) {
    res.json((await pg.query('SELECT * FROM sci_user;')).rows[0]);
  });

  //send data
  app.get('/data.dat.json', function (req, res) {
    res.sendFile(`${__dirname}/data.dat.json`)
  });

  //send user
  app.get('/user', function (req, res) {
    res.sendFile(`${__dirname}/user.json`)
  });

  app.post('/:type/post', function (req, res) {
    req.body.user_id
    req.body.is_offering
    req.send.body.data
  })

  app.post('/createUser', function (req, res) {
    var name = req.body.name;
    var password = req.body.password;
    var location = req.body.location;
    if(name && password && location){  // TODO: Add further input verification
      databaseWrapper.createUser(pg, name, password, location).then(result => {
        res.send(result)
      })
    }
  })

  app.post('/sharefood', function (req, res) {
    req.body.offer
    req.body.eigenschaften
    req.body.name
    req.body.address
    console.log("body" + req.body)
    console.log("body" + req.body.name)
    let sql = "INSERT INTO sci_inventory(userID, username, item_type, item_name, item_data, item_location, item_location_human_readable_address, item_timestamp, isOffer) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9)"
    let values = [1,req.body["name"], 'food', req.body.offer, {"item_type":req.body.eigenschaften}, '(48.39723,9.99393)', req.body.address, new Date().toISOString(), 'True' ]
    //console.log(req);
    pg.query({text:sql,values:values}).then(result => {
      res.json(result.rows[0])
      //console.log(result.rows[0])
    })
  })



  app.use('/images', express.static(path.join(__dirname, "images")))

  app.get('/inventory', function (req, res) {
    databaseWrapper.getInventoryList(pg).then(result => {
      res.json(result)
    });
  });

  app.get('/inventory/claim/:id', function (req, res) {
    // TODO: Check auth status :D

    let inventoryID = req.params.id;
    console.log(inventoryID);
    if (inventoryID === void 0) {
      res.json(false);
      return;
    }

    inventoryID = Number.parseInt(inventoryID);

    console.log(inventoryID);

    res.json(databaseWrapper.claimInventory(pg, inventoryID))
  });

  app.post("/login", (req, res) => {
    auth.login(req.body.username, req.body.password).then(result => {
      res.send(result)
    });
  })

  app.listen(8888, () => {
    log("Server is ready for requests!");
  });
})();